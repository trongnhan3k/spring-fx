package vn.vccorp.adtech.analytics;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import vn.vccorp.adtech.analytics.util.config.Constant;

import java.lang.management.ManagementFactory;
import java.util.Properties;

/**
 * Created by nhanvt on 2020/02/06
 */
@SpringBootApplication
public class TestServiceMain extends Application {
    private ConfigurableApplicationContext context;

    private Parent rootNode;
    protected static final Logger logger = LoggerFactory.getLogger(TestServiceMain.class);

//    public static void main(String[] args) throws Exception {
//        startService(args);
//    }
//
//    private static void startService(String[] args) {
//        Properties pros = new Properties();
//        pros.put("server.port", Constant.SERVICE_PORT);
//
//        SpringApplicationBuilder applicationBuilder = new SpringApplicationBuilder()
//                .sources(TestServiceMain.class)
//                .properties(pros);
//        applicationBuilder.run(args);
//
//        logger.info("Service start at port {}\t{}", Constant.SERVICE_PORT, ManagementFactory.getRuntimeMXBean().getName());
//    }

    @Override
    public void init() throws Exception {

        SpringApplicationBuilder builder = new SpringApplicationBuilder(TestServiceMain.class);

        context = builder.run(getParameters().getRaw().toArray(new String[0]));

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/main.fxml"));

        loader.setControllerFactory(context::getBean);

        rootNode = loader.load();

        logger.info("Service start at port {}\t{}", Constant.SERVICE_PORT, ManagementFactory.getRuntimeMXBean().getName());
    }

    @Override

    public void start(Stage primaryStage) throws Exception {

        Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();

        double width = visualBounds.getWidth();

        double height = visualBounds.getHeight();

        primaryStage.setScene(new Scene(rootNode, width, height));

        primaryStage.centerOnScreen();

        primaryStage.show();

    }

    @Override

    public void stop() throws Exception {

        context.close();

    }

}
