package vn.vccorp.adtech.analytics.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import vn.vccorp.adtech.analytics.util.DateUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by tudv on 2019/08/23
 */
public abstract class BaseDAO {
    protected static final Logger eLogger = LoggerFactory.getLogger("ErrorLog");
    protected static final Logger logger = LoggerFactory.getLogger("CommonLog");

    protected void releaseResource(PreparedStatement ps, ResultSet rs) {
        try {
            if (ps != null) {
                ps.clearBatch();
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        } catch (SQLException e) {

        }
    }

    protected void releaseConn(Connection conn) {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
            eLogger.error("ERROR closing conn ", e);
        }
    }

    protected abstract Connection getConnection(boolean autoCommit);

    protected abstract Connection getConnection();

    protected Long getStartTime(String fromDate) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateUtils.df.parse(fromDate));
        Long startTime = calendar.getTime().getTime();
        return startTime;
    }

    public Long getEndTime(String toDate) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateUtils.df.parse(toDate));
        calendar.add(Calendar.HOUR, 23);
        Long endTime = calendar.getTime().getTime();
        return endTime;
    }
}
