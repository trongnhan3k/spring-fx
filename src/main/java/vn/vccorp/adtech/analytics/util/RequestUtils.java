package vn.vccorp.adtech.analytics.util;

import okhttp3.*;
import vn.vccorp.adtech.analytics.base.Base;
import vn.vccorp.adtech.analytics.util.config.Constant;

import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * Created by tudv on 2019/05/30
 */
public class RequestUtils extends Base {
    private static OkHttpClient clientHttps;
    private static MediaType mediaType;

    private static OkHttpClient okHttpClient;

    static {
        ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                .tlsVersions(TlsVersion.TLS_1_2)
                .cipherSuites(
                        CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
                        CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256)
                .build();

        clientHttps = new OkHttpClient.Builder()
                .connectionSpecs(Collections.singletonList(spec))
                .connectTimeout(Constant.REQUEST_TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(Constant.REQUEST_TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(Constant.REQUEST_TIME_OUT, TimeUnit.SECONDS)
                .build();

        okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(Constant.REQUEST_TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(Constant.REQUEST_TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(Constant.REQUEST_TIME_OUT, TimeUnit.SECONDS)
                .build();

//        mediaType = MediaType.parse("application/mediaType; charset=utf-8");
    }

    public static String sendGetRequest(String url) {
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        try {
            Response response = okHttpClient.newCall(request).execute();
//            GlobalObjects.totalRequest.updateAndGet(x -> x + 1);
            cLogger.info("Make {} request to \t{}", request.method(), request.url());
            return response.body().string();
        } catch (Exception e) {
            eLogger.info("error when request [{}]", e.getMessage());
            e.printStackTrace();
            return null;
        }
    }

}
