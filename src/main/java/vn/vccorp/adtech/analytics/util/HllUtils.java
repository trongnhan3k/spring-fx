package vn.vccorp.adtech.analytics.util;

import com.clearspring.analytics.stream.cardinality.HyperLogLogPlus;
import vn.vccorp.adtech.analytics.base.Base;

import java.util.List;

/**
 * Created by tudv on 2019/08/27
 */
public class HllUtils extends Base {
    public long estimateTotalUser(List<HyperLogLogPlus> users) {
        if (users == null) return 0;

        HyperLogLogPlus hyperLogLogPlus = new HyperLogLogPlus(16, 25);
        for (HyperLogLogPlus user : users) {
            try {
                hyperLogLogPlus.addAll(user);
            } catch (Exception e) {
                eLogger.error("error estimate total user {}", e.getMessage());
            }
        }
        return estimateUser(hyperLogLogPlus);
    }

    public long estimateUser(HyperLogLogPlus user) {
        if(user != null) {
            return user.cardinality();
        }else{
            return 0;
        }
    }
}
