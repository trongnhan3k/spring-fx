package vn.vccorp.adtech.analytics.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by nhanvt on 2020/02/05
 */
public class DateUtils {
    private static final Logger logger = LoggerFactory.getLogger(DateUtils.class);
    public static final String dfString = "yyyy-MM-dd";
    public static final String hfString = "yyyy-MM-dd HH:mm:SS";
    public static final SimpleDateFormat df = new SimpleDateFormat(dfString);
    public static final SimpleDateFormat hf = new SimpleDateFormat(hfString);

    public static boolean isThisDateValid(String dateFromat, String... dateToValidate) {
        if (dateToValidate == null) {
            return false;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
        sdf.setLenient(false);

        try {
            //if not valid, it will throw ParseException
            for (String date : dateToValidate) {
                if (Utils.checkParam(date))
                    sdf.parse(date);
                else return false;
            }

        } catch (ParseException e) {
            return false;
        }

        return true;
    }

    public static List<String> getListDate(String fromDate, String toDate) {

        List<String> lst = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        try {
            Date date = df.parse(fromDate);
            calendar.setTime(date);
            while (df.format(calendar.getTime()).compareTo(toDate) <= 0) {
                lst.add(df.format(calendar.getTime()));
                calendar.add(Calendar.DAY_OF_MONTH, 1);

            }
        } catch (ParseException e) {
            logger.error("error get list date from_date =" + fromDate + " to_date=" + toDate, e);
        }
        return lst;
    }

    public List<Long> getListHour(String fromDate, String toDate) {
        List<Long> lst = new ArrayList<>();
        try {
            Long fromDateL = getStartTime(fromDate);
            Long toDateL = getEndTime(toDate);
            for (long i = fromDateL; i <= toDateL; i += 3600000) {
                lst.add(i);
            }
        } catch (ParseException e) {
            logger.error("error get list date from_date =" + fromDate + " to_date=" + toDate, e);
        }
        return lst;
    }

    public List<Long> getListDateLong(String fromDate, String toDate) {
        List<Long> lst = new ArrayList<>();
        try {
            Long fromDateL = getStartTime(fromDate);
            Long toDateL = getEndTime(toDate);
            for (long i = fromDateL; i <= toDateL; i += 86400000) {
                lst.add(i);
            }
        } catch (ParseException e) {
            logger.error("error get list date from_date =" + fromDate + " to_date=" + toDate, e);
        }
        return lst;
    }

    public static List<String> getListDate(String date, int delta) {

        List<String> lst = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(df.parse(date));
            for (int i = 0; i < delta; i++) {
                lst.add(df.format(calendar.getTime()));
                calendar.add(Calendar.DAY_OF_MONTH, -1);
            }
        } catch (ParseException e) {
            logger.error("error get list date date =" + date + " delta=" + delta, e);
        }
        return lst;
    }

    public static List<String> getListDate(String fromDate, String toDate, Integer timeDelta) {

        List<String> lst = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH");
        try {
            Date date = df.parse(fromDate);
            calendar.setTime(date);
            while (df.format(calendar.getTime()).compareTo(toDate) <= 0) {
                lst.add(df.format(calendar.getTime()));
                calendar.add(Calendar.HOUR_OF_DAY, timeDelta);
            }
        } catch (ParseException e) {
            logger.error("error get list date from_date =" + fromDate + " to_date=" + toDate, e);
        }
        return lst;
    }

    public static String convertTimeLong(Long time, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(new Date(time));
    }

    public static List<String> getListHourly(Long startTime, int range) {
        SimpleDateFormat df = new SimpleDateFormat(hfString);
        String hourString = convertTimeLong(startTime, hfString);
        List<String> lst = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();

        try {
            calendar.setTime(df.parse(hourString));
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            for (int i = 0; i < range; i++) {
                lst.add(df.format(calendar.getTime()));
                calendar.add(Calendar.HOUR_OF_DAY, 1);
            }
        } catch (ParseException e) {
            logger.error("error get list hour hour =" + hourString + " delta=" + range, e);
        }
        return lst;
    }

    public static Long timeLongToHour(Long timeLong) {
        long tmp = timeLong % (60 * 60 * 1000);
        return timeLong - tmp;
    }

    public static String convertHourLong(Long time) {
        Date date = new Date(time);
        DateFormat formatter = new SimpleDateFormat("HH");
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        String dateFormatted = formatter.format(date);
        return dateFormatted;
    }

    public Long getStartTime(String fromDate) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateUtils.df.parse(fromDate));
        Long startTime = calendar.getTime().getTime();
        return startTime;
    }

    public Long getTimeLong(String date) throws ParseException {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(DateUtils.hf.parse(date));
            Long time = calendar.getTime().getTime();
            return time;
    }

    public Long getEndTime(String toDate) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(DateUtils.df.parse(toDate));
        calendar.add(Calendar.HOUR, 23);
        Long endTime = calendar.getTime().getTime();
        return endTime;
    }

    public String getDateSql(long dt) {

        Date date = new Date(dt);
        return df.format(date);
    }

    public long timeLongToDay(long timeLong) {
        long dt = timeLongToHour(timeLong);
        return dt - ((dt + 25200000) % 86400000);
    }

    public long timeLongToHour(long timeLong) {
        long tmp = timeLong % (60 * 60 * 1000);
        return timeLong - tmp;
    }
}