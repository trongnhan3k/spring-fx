package vn.vccorp.adtech.analytics.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    private static final Logger logger = LoggerFactory.getLogger(FileUtils.class);

    public List<String> getListName(String directory) {
        List<String> names = new ArrayList<>();
        File folder = new File(directory);
        File[] listOfFiles = folder.listFiles();
        for (File file : listOfFiles) {
            if (file.isFile()) {
                names.add(file.getPath());
            }
        }

        return names;
    }

    public List<String> readFile(String fileName) {
        BufferedReader br = null;
        FileReader fr = null;
        List<String> contents = new ArrayList<>();
        try {
            fr = new FileReader(fileName);
            br = new BufferedReader(fr);
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                contents.add(sCurrentLine);
            }
        } catch (IOException e) {
            logger.error("error read file ", e);
        } finally {
            try {
                if (br != null) br.close();
                if (fr != null) fr.close();
            } catch (IOException ex) {
                logger.error("error read file ", ex);
            }
        }

        return contents;
    }

    public static boolean writeToFile(String fileName, List<String> lines) {
        Charset utf8 = StandardCharsets.UTF_8;
        try {
            Files.write(Paths.get(fileName), lines, utf8, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            return true;
        } catch (IOException e) {
            logger.error("error when write file", e);
            return false;
        }
    }
}
