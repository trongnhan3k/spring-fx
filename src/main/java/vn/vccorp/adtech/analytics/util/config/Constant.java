package vn.vccorp.adtech.analytics.util.config;

public interface Constant {
    int SERVICE_PORT = ConfigurationManager.getInstance().getInt("server.port");
    int threadSleep = ConfigurationManager.getInstance().getInt("thread.minute.sleep");
    int provider = ConfigurationManager.getInstance().getInt("provider.type");
    int limit = ConfigurationManager.getInstance().getInt("default.limit");
    int offset = ConfigurationManager.getInstance().getInt("default.offset");
    int REQUEST_TIME_OUT = 3;
    int pegaId = 2601;
    int k14 = 2046;

    String PHOENIX_URL = ConfigurationManager.getInstance().getString("phoenix.url");
    String TABLE_NOTIFY_TOTALNOTI = ConfigurationManager.getInstance().getString("phoenix.table.notify.total");
    String TABLE_TOTAL_BY_NOTIFY_DAILY = ConfigurationManager.getInstance().getString("phoenix.table.notify.byId.daily");
    String TABLE_TOTAL_BY_NOTIFY_HOURLY = ConfigurationManager.getInstance().getString("phoenix.table.notify.byId.hourly");

    String API_GET_TOTAL_SENT = ConfigurationManager.getInstance().getString("api.get.total.sents");
    String API_GET_DATA_MINH = ConfigurationManager.getInstance().getString("api.get.data.Minh");
}
