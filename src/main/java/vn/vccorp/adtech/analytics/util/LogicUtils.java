package vn.vccorp.adtech.analytics.util;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * Created by tudv on 2019/08/26
 */
public class LogicUtils {
    public String convertCardTypeToPost(int cardType) {
        String post = "new_post";
        switch (cardType) {
            case 11:
            case 10: {
                post = "news_post";
                break;
            }
            case 1:
            case 9: {
                post = "picture";
                break;
            }
            case 6: {
                post = "album";
                break;
            }

            case 12:
            case 13:
            case 15: {
                post = "share";
                break;
            }

            case 2:
            case 4:
            case 14: {
                post = "video";
                break;
            }
            default: {
                post = "news_post";
                break;
            }
        }
        return post;
    }

    public static boolean checkStringNull(String input) {
        if (input == null || input.trim().isEmpty()) {
            return true;
        } else return false;
    }

    public String getEncodeHMACSHA256(String data, String secretKey) throws NoSuchAlgorithmException, InvalidKeyException {
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(secretKey.getBytes(), "HmacSHA256");
        sha256_HMAC.init(secret_key);
        byte[] bytes = sha256_HMAC.doFinal(data.getBytes());
        String hash = new String(Base64.getEncoder().encode(bytes));
        return hash;
    }
}
