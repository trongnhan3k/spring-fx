package vn.vccorp.adtech.analytics.util.config;


import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesLoader {
    public static Properties loadAllPropertiesFromFile(String filePath) throws Exception {
        Properties prop = new Properties();
        InputStream input = null;

        input = new FileInputStream(filePath);
        // load a properties file
        prop.load(input);
        return prop;
    }
}
