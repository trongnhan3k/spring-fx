package vn.vccorp.adtech.analytics.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.lang.reflect.Type;
import java.net.URI;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.CRC32;

/**
 * Created by nguye on 22/07/2016.
 */
public class Utils {
    private static final Logger logger = LoggerFactory.getLogger(Utils.class);
    SimpleDateFormat sdfFull = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat Hour = new SimpleDateFormat("HH");

    public static boolean checkParam(String param) {
        if (param != null && !param.equals("")) {
            return true;
        } else return false;
    }

    public String getTimeString(long milis) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(milis);
        return sdf.format(c.getTime());
    }

    //    SimpleDateFormat Hour = new SimpleDateFormat("HH");
    public String getHourString(long milis) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(milis);
        return Hour.format(c.getTime());
    }

    public Integer getHourInt(long milis) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(milis);
        return Integer.valueOf(Hour.format(c.getTime()));
    }

    public static String getDateToday() {
        return sdf.format(Calendar.getInstance().getTime());
    }

    public static String getDateYesterday() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -1);
        return sdf.format(c.getTime());
    }


    public Long getItemId(String url) {
//        String result = "-1";
//        String pattern = "-\\d+\\.";
//        Pattern r = Pattern.compile(pattern);
//        Matcher matcher = r.matcher(url);
//        if (matcher.find()) {
//            String temp = matcher.group(0);
//            result = temp.substring(1, temp.length() - 1);
////            System.out.println("Found value: " + matcher.group(1) );
////            System.out.println("Found value: " + matcher.group(2) );
//        }
//        return Long.parseLong(result);
        String dataSplit = url.split("\\.")[0];
        String[] tmp = dataSplit.split("-");
        dataSplit = tmp[tmp.length - 1];


        dataSplit = dataSplit.substring(dataSplit.indexOf("r") + 1);
        dataSplit = dataSplit.substring(dataSplit.indexOf("/") + 1);


        long result = -1L;
        try {
            result = Long.parseLong(dataSplit);
        } catch (Exception ignored) {

        }
        if (result < 100000) {
            try {
                CRC32 crc32 = new CRC32();
                URI path = URI.create(url);
                String path_ = path.getPath();
                crc32.update(path_.getBytes());
                result = crc32.getValue();
            } catch (Exception ignored) {

            }
        }
        return result;
    }


    public Long getItem(String path) {
        Long result = -1L;
        try {
            String pattern = "([0-9]{6,}+)";
            Pattern compile = Pattern.compile(pattern);
            Matcher matcher = compile.matcher(path);
            while (matcher.find()) {
                String item = matcher.group(0);
                result = Long.parseLong(matcher.group(0));
                if (path.contains(item + ".")) {
                    break;
                }
            }
            if (result < 100000) {
                CRC32 crc32 = new CRC32();
//                String dataEncode = java.net.URLEncoder.encode(path.split("\\?")[0].split("#")[0].split("&")[0].replaceAll(" ", ""), "UTF-8");

                String dataTmp = path.split("\\?")[0].split("#")[0].split("&")[0].replaceAll(" ", "");

                if (!dataTmp.endsWith("/") && !dataTmp.endsWith("%2F")) {
                    dataTmp = dataTmp + "/";
                }
                String dataEncode = java.net.URLEncoder.encode(dataTmp, "UTF-8");

                crc32.update(dataEncode.getBytes());
                result = crc32.getValue();
            }
            return result;
        } catch (Exception e) {
            return -1L;
        }
    }


    public String[] fillFullArray(String[] input, int logLength) {
        String[] result = new String[logLength];

        for (int i = input.length; i < logLength; i++) {
            result[i] = "-1";
        }

        try {
            for (int i = 0; i < input.length; i++) {
                result[i] = input[i];
            }
        } catch (Exception e) {
            logger.error("error when fill array " + toMessageString(input) + "\n" + e);
        }


        return result;
    }

    private String toMessageString(String log[]) {
        String a = "";

        for (String x : log) {
            a += x + "\t";
        }

        return a;
    }

    public static List<String> convertStringToList(String strings) {
        Gson gson = new Gson();

        Type type = new TypeToken<List<String>>() {
        }.getType();
        List<String> result = gson.fromJson(strings, type);

        return result;
    }

    public static Integer randomInt(int leftLimit, int rightLimit) {
        return leftLimit + (int) (new Random().nextFloat() * (rightLimit - leftLimit));
    }

    public static long generateTimestamp(long userID) {

        // Warning: Don't change these value
        final int TOTAL_BITS = 64;
        final int EPOCH_BITS = 44;
        final int NODE_ID_BITS = 10;
        final int SEQUENCE_BITS = 10;

        // Human time (GMT): Monday, 25 February 2019 02:41:37
        // Warning: Don't change this value
        final long CUSTOM_EPOCH = 1551062497000L;
        long localTime = userID >>> (TOTAL_BITS - EPOCH_BITS);
        long timeInMilliSecs = CUSTOM_EPOCH + localTime;
        return timeInMilliSecs;
    }

    public List<String> deserial(ByteBuffer byteBuffer) throws Exception {
        ByteArrayInputStream bais = new ByteArrayInputStream(byteBuffer.array());
        ObjectInputStream ios = new ObjectInputStream(bais);
        Object body = ios.readObject();

        List<String> lst = Arrays.asList((String[]) body);
        return lst;
    }
}
