package vn.vccorp.adtech.analytics.base;


import vn.vccorp.adtech.analytics.bo.request.RequestParam;
import vn.vccorp.adtech.analytics.global.GlobalObject;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by tudv on 2019/09/14
 */
public abstract class BaseController extends Base {
    //    protected static final Logger requestLog = LogManager.getLogger("RequestLog");
    protected String getResponse(Object baseReponse) {
        return GlobalObject.gsonNull.toJson(baseReponse);
    }

    protected RequestParam getRequestParam(HttpServletRequest request) {
        RequestParam requestParam = new RequestParam();
        requestParam.setFromDate(request.getParameter("fromDate"));
        requestParam.setToDate(request.getParameter("toDate"));
        requestParam.setOrderId(request.getParameter("notifyId"));
        requestParam.setTimeType(request.getParameter("timeType"));
        requestParam.setUserId(request.getParameter("userId"));
        requestParam.setCategoryId(request.getParameter("categoryId"));
        requestParam.setHour(request.getParameter("hour"));
        requestParam.setTotalSents(request.getParameter("totalSents"));
        requestParam.setSourceId(request.getParameter("sourceId"));
        if (request.getParameter("timeSents") != null) {
            requestParam.setSentTime(Long.parseLong(request.getParameter("timeSents")));
        }

        try {
            requestParam.setLimit(Long.parseLong(request.getParameter("limit")));
        } catch (Exception ignored) {
        }
        try {
            requestParam.setOffset(Long.parseLong(request.getParameter("offset")));
        } catch (Exception ignored) {
        }

        return requestParam;
    }
}
