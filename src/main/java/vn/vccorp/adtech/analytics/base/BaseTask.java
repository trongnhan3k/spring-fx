package vn.vccorp.adtech.analytics.base;

/**
 * Created by tudv on 2019/08/28
 */
public abstract class BaseTask extends Base implements Runnable {
    @Override
    public abstract void run();
}
