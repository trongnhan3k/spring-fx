package vn.vccorp.adtech.analytics.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by tudv on 2019/08/23
 */
public abstract class Base {
    protected static final Logger eLogger = LoggerFactory.getLogger("ErrorLog");
    protected static final Logger cLogger = LoggerFactory.getLogger("CommonLog");
}
